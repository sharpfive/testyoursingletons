//
//  ViewControllerTests.swift
//  TestYourSingletons
//
//  Created by Jaim Zuber on 1/27/15.
//  Copyright (c) 2015 Sharp Five Software. All rights reserved.
//

import UIKit
import XCTest
import TestYourSingletons

class ViewControllerTests: XCTestCase {
    
    var sut: ViewController!
    var scaryStub: ScaryStub!
    
    class ScaryStub: ScarySingleton {
        var doSomethingImportantCalled: Bool = false
        
        override func doSomethingImportant() {
            doSomethingImportantCalled = true
        }
    }

    override func setUp() {
        super.setUp()
        scaryStub = ScaryStub()
        sut = ViewController(scarySingleton: scaryStub)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDoSomethingImportantIsCalled() {
        // This is an example of a functional test case.
        let view = sut.view
        
        XCTAssertTrue(scaryStub.doSomethingImportantCalled)
    }

    func testImportantValueIsSet() {
        let view = sut.view
        XCTAssertEqual(scaryStub.someimportantValue!, "So important!")
    }
}
