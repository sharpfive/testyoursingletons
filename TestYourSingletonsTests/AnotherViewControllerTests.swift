//
//  AnotherViewControllerTests.swift
//  TestYourSingletons
//
//  Created by Jaim Zuber on 1/31/15.
//  Copyright (c) 2015 Sharp Five Software. All rights reserved.
//

import UIKit
import XCTest
import TestYourSingletons

class AnotherViewControllerTests: XCTestCase {
    
    var storyboard: UIStoryboard!
    var sut: AnotherViewController!
    var scaryStub: ScaryStub!

    class ScaryStub: ScarySingleton {
        var doSomethingImportantCalled: Bool = false
        
        override func doSomethingImportant() {
            doSomethingImportantCalled = true
        }
    }
    
    override func setUp() {
        super.setUp()
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewControllerWithIdentifier("AnotherViewController") as? AnotherViewController
        scaryStub = ScaryStub()
        // we inject our dependency here
        sut.scarySingleton = scaryStub
        sut.importantStuff()
    }
 
    func testDoSomethingImportantIsCalled() {
        XCTAssertTrue(scaryStub.doSomethingImportantCalled)
    }
    
    func testImportantValueIsSet() {
        XCTAssertEqual(scaryStub.someimportantValue!, "So important!")
    }
}
