//
//  AnotherViewController.swift
//  TestYourSingletons
//
//  Created by Jaim Zuber on 1/31/15.
//  Copyright (c) 2015 Sharp Five Software. All rights reserved.
//

import UIKit

public class AnotherViewController: UIViewController {

    public var scarySingleton = ScarySingleton.sharedInstance
        
    public func importantStuff() {
        // So testable!
        scarySingleton.doSomethingImportant()
        scarySingleton.someimportantValue = "So important!"
    }
}
