//
//  ViewController.swift
//  TestYourSingletons
//
//  Created by Jaim Zuber on 1/27/15.
//  Copyright (c) 2015 Sharp Five Software. All rights reserved.
//

import UIKit

public class ViewController: UIViewController {
    
    // We set the scarySingleton here for most use cases
    private let scarySingleton = ScarySingleton.sharedInstance
    
    public init(scarySingleton: ScarySingleton) {
        // test code injects it's dependencies
        self.scarySingleton = scarySingleton
        super.init(nibName: nil, bundle: nil)
    }

    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // So testable!
        scarySingleton.doSomethingImportant()
        scarySingleton.someimportantValue = "So important!"
    }
}

