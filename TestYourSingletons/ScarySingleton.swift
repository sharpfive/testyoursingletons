//
//  ScarySingleton.swift
//  TestYourSingletons
//
//  Created by Jaim Zuber on 1/27/15.
//  Copyright (c) 2015 Sharp Five Software. All rights reserved.
//

import Foundation

public class ScarySingleton {
    
    class var sharedInstance: ScarySingleton {
        
        struct Static {
            static let instance: ScarySingleton = ScarySingleton()
        }
        return Static.instance
    }
    
    public init() {
        
    }
    
    public var someimportantValue: String? = nil
    
    public func doSomethingImportant() {
        NSLog("So important")
    }
}
